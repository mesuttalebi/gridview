﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace mesoft.gridview.Models
{
    public class Customer
    {
        public int Id { get; set; }        
        public string CompanyName { get; set; }        
        public string ContactTitle { get; set; }
        public string Address { get; set; }
        public string City { get; set; }               
        public string Country { get; set; }
        public string Phone { get; set; }        
    }
}